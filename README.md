<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 04-30-2024 and will be end of life on 04-30-2025. The capabilities of this Pre-Built have been replaced by the [Atlassian - Jira - REST](https://gitlab.com/itentialopensource/pre-built-automations/atlassian-jira-rest)

<!-- Update the below line with your Pre-Built name -->
# Jira Issue Creation

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
  * [Operations Manager and JSON-Form](#operations-manager-and-json-form)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
  * [Input Variables](#input-variables)
* [Additional Information](#additional-information)

## Overview
This Pre-Built integrates with the [Jira Open Source Adapter](https://gitlab.com/itentialopensource/adapters/itsm-testing/adapter-jira) to create an issue within Jira.

## Operations Manager and JSON-Form

This workflow has an [Operations Manager Item](./bundles/automations/Jira%20Issue%20Creation.json) that calls a workflow. The Ops Manager Item uses a JSON-Form to specify common fields populated when an issue is created. The workflow the Ops Manager item calls queries data from the formData job variable.

## Installation Prerequisites

Users must satisfy the following pre-requisites:

- Itential Automation Platform
  - `^2022.1`
- [Jira Open Source Adapter](https://gitlab.com/itentialopensource/adapters/itsm-testing/adapter-jira)
  - `^1.7.2`

## Requirements

This Pre-Built requires the following:

- A [Jira Open Source Adapter](https://gitlab.com/itentialopensource/adapters/itsm-testing/adapter-jira).
- A Jira account that has permission to create an issue.

## Features

The main benefits and features of the Pre-Built are outlined below.

* Allows user to create a Jira issue (Task, Bug, Story or Epic) for the specified project and also: 
  * set the summary and description of the issue
  * set the status of the issue
  * set priority of the issue  
  * add a label of the issue

## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button.

## How to Run

Use the following to run the Pre-Built:

* Run the Operations Manager Item `Jira Issue Creation` or call [Jira Issue Creation](./bundles/workflows/Jira%20Issue%20Creation.json) from your workflow as a child job.

### Input Variables
_Example_

```json
{
  "projectKey": "Specific Project key under which issue should be created",
  "description": "This is a description of the Jira issue",
  "summary": "Short summary or title for the Jira issue",
  "issueType": "One of the four issue types such as Task, Story, Bug or Epic",
  "priority": "One of the four priorities such as Lowest, Low, High or Highest",
  "label": "Jira label associated with issue"
}
```
## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
