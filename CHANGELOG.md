
## 0.0.20 [07-11-2024]

Add deprecation notice and metadata

See merge request itentialopensource/pre-built-automations/jira-issue-creation!9

2024-07-11 13:40:36 +0000

---

## 0.0.19 [06-21-2022]

* patch/DSUP-1361

See merge request itentialopensource/pre-built-automations/jira-issue-creation!7

---

## 0.0.18 [12-21-2021]

* Patch/dsup 1046

See merge request itentialopensource/pre-built-automations/jira-issue-creation!6

---

## 0.0.17 [11-15-2021]

* Update pre-built description

See merge request itentialopensource/pre-built-automations/jira-issue-creation!5

---

## 0.0.16 [07-07-2021]

* Update README.md, package.json files

See merge request itentialopensource/pre-built-automations/jira-issue-creation!4

---

## 0.0.15 [05-11-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/jira-issue-creation!1

---

## 0.0.14 [05-05-2021]

* Update images/jira_issue_creation_canvas.png,...

See merge request itential/sales-engineer/selabprebuilts/jira-issue-creation!9

---

## 0.0.13 [03-23-2021]

* Update images/jira_issue_creation_canvas.png,...

See merge request itential/sales-engineer/selabprebuilts/jira-issue-creation!9

---

## 0.0.12 [03-02-2021]

* Update images/jira_issue_creation_canvas.png,...

See merge request itential/sales-engineer/selabprebuilts/jira-issue-creation!9

---

## 0.0.11 [03-02-2021]

* patch/2021-03-02T13-35-34

See merge request itential/sales-engineer/selabprebuilts/jira-issue-creation!8

---

## 0.0.10 [02-24-2021]

* Replace CreateJiraIssueAutoStudio.png

See merge request itential/sales-engineer/selabdemos/create-issuejira!5

---

## 0.0.9 [02-23-2021]

* Replace CreateJiraIssueAutoStudio.png

See merge request itential/sales-engineer/selabdemos/create-issuejira!5

---

## 0.0.8 [02-23-2021]

* Update README.md

See merge request itential/sales-engineer/selabdemos/create-issuejira!4

---

## 0.0.7 [02-23-2021]

* Update package.json

See merge request itential/sales-engineer/selabdemos/create-issuejira!1

---

## 0.0.6 [02-23-2021]

* Update package.json

See merge request itential/sales-engineer/selabdemos/create-issuejira!1

---

## 0.0.5 [02-23-2021]

* Update package.json

See merge request itential/sales-engineer/selabdemos/create-issuejira!1

---

## 0.0.4 [02-22-2021]

* Update package.json

See merge request itential/sales-engineer/selabdemos/create-issuejira!1

---

## 0.0.3 [02-17-2021]

* Update package.json

See merge request itential/sales-engineer/selabdemos/create-issuejira!1

---

## 0.0.2 [02-17-2021]

* Bug fixes and performance improvements

See commit 91438a6

---\n
